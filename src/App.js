import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './assets/style/global.css';
import './App.css';
import Header from './global/header/Header';
import Home from './pages/home/Home';
import SubCat from './pages/subcat/SubCat';
import Search from './pages/search/Search';


function App() {
  return (
    <div className='Main'>
      <Header />
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/details" element={<SubCat />} />
          <Route path="/search" element={<Search />} />
        </Routes>
      </Router>
    </div>

  );
}

export default App;
