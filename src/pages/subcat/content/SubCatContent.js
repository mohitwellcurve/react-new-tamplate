import React from 'react';

import "./SubCatContent.css";

function SubCatContent(props) {
    return (
        <div className='text-section custome-scroll'>
            <ul>

                <li>
                    <span><img src={require('../../../assets/images/home/cmpny-logo.jpg')} /> <a href='#'>google</a></span>
                    <img src={require('../../../assets/images/home/image.jpg')} className='test small-img' />
                    <h4>MongoDB Atlas</h4>
                    <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                    <div className='btn-side'>
                        <button>explore</button>
                        <button className='view'>view website</button>
                    </div>
                </li>

                <li>
                    <span><img src={require('../../../assets/images/home/cmpny-logo.jpg')} /> <a href='#'>google</a></span>
                    <h4>MongoDB Atlas</h4>
                    <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                    <div className='btn-side'>
                        <button>explore</button>
                    </div>
                </li>

                <li>
                    <span><img src={require('../../../assets/images/home/cmpny-logo.jpg')} /> <a href='#'>google</a></span>
                    <img src={require('../../../assets/images/home/image.jpg')} className='test small-img' />
                    <h4>MongoDB Atlas</h4>
                    <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                    <div className='btn-side'>
                        <button>explore</button>
                    </div>
                </li>

                <li>
                    <span><img src={require('../../../assets/images/home/cmpny-logo.jpg')} /> <a href='#'>google</a></span>
                    <h4>MongoDB Atlas</h4>
                    <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                    <div className='btn-side'>
                        <button>explore</button>
                        <button className='view'>view website</button>
                    </div>
                </li>

            </ul>
        </div>
    );
}

export default SubCatContent;
