import React from 'react';

import "./SubCat.css"
import SubCatContent from './content/SubCatContent';

import $ from "jquery"
window.jQuery = $;

function SubCat(props) {

    $(".Details_side .inner li").click(function () {
        $('.inner li').removeClass('li-active')
        $(this).addClass('li-active')
    });

    return (
        <div className='Details_section'>
            <div className='container'>
                <div className='Details_side'>
                    <div className='row'>
                        <div className='left-side col-md-4'>
                            <div className='inner'>
                                <h3>Category Name</h3>
                                <ul>
                                    <li>
                                        <a href='javascript:void(0)'>
                                            <button>Lorem Ipsum is simply dummy text</button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0)'>
                                            <button>Lorem Ipsum is simply dummy text</button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0)'>
                                            <button>Lorem Ipsum is simply dummy text</button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0)'>
                                            <button>Lorem Ipsum is simply dummy text</button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0)'>
                                            <button>Lorem Ipsum is simply dummy text</button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0)'>
                                            <button>Lorem Ipsum is simply dummy text</button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0)'>
                                            <button>Lorem Ipsum is simply dummy text</button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0)'>
                                            <button>Lorem Ipsum is simply dummy text</button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0)'>
                                            <button>Lorem Ipsum is simply dummy text</button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0)'>
                                            <button>Lorem Ipsum is simply dummy text</button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0)'>
                                            <button>Lorem Ipsum is simply dummy text</button>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className='right-side col-md-8'>
                            <div className='text'>
                                <SubCatContent />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default SubCat;