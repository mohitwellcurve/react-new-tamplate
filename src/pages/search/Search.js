import React from 'react';

import "./Search.css"

function Search() {
    return (
        <div className="Search_section">
            <div className='container'>
                <div className='row'>
                    <div className='right-side col-md-12'>
                        <div className='text'>
                            <div className='text-section text-section-copy custome-scroll'>
                                <ul>
                                    <li>
                                        <span><img src={require('../../assets/images/home/cmpny-logo.jpg')} /> <a href='#'>google</a></span>
                                        <img src={require('../../assets/images/home/image.jpg')} className='test small-img' />
                                        <h4>MongoDB Atlas</h4>
                                        <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                                    </li>
                                    <li>
                                        <span><img src={require('../../assets/images/home/cmpny-logo.jpg')} /> <a href='#'>google</a></span>
                                        <img src={require('../../assets/images/home/image.jpg')} className='test small-img' />
                                        <h4>MongoDB Atlas</h4>
                                        <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                                    </li>
                                    <li>
                                        <h4>Lorem Ipsum</h4>
                                        <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                                    </li>
                                    <li>
                                        <h4>Lorem Ipsum</h4>
                                        <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                                    </li>
                                    <li>
                                        <h4>Lorem Ipsum</h4>
                                        <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                                    </li>
                                    <li>
                                        <h4>Lorem Ipsum</h4>
                                        <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                                    </li>
                                    <li>
                                        <h4>Lorem Ipsum</h4>
                                        <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                                    </li>
                                    <li>
                                        <h4>Lorem Ipsum</h4>
                                        <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                                    </li>
                                    <li>
                                        <h4>Lorem Ipsum</h4>
                                        <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                                    </li>
                                    <li>
                                        <h4>Lorem Ipsum</h4>
                                        <p>This tutorial will give you great understanding on <a href='#'>MongoDB</a> concepts needed to create and deploy a highly scalable and performance-oriented database. Audience....</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Search;
