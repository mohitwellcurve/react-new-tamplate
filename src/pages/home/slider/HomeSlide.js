import React from 'react';
import Slider from 'react-slick';

import "../slider/HomeSlide.css";
import SlideContent from './content/SlideContent';

function SampleNextArrow(props) {
	const { className, style, onClick } = props;
	return (
		<div
			className={className}
			style={{ ...style, display: "block", background: "#36c991" }}
			onClick={onClick}
		/>
	);
}

function SamplePrevArrow(props) {
	const { className, style, onClick } = props;
	return (
		<div
			className={className}
			style={{ ...style, display: "block", background: "#36c991" }}
			onClick={onClick}
		/>
	);
}

const settings = {
	dots: false,
	infinite: true,
	arrows: true,
	speed: 500,
	slidesToShow: 3,
	slidesToScroll: 1,
	nextArrow: <SampleNextArrow />,
	prevArrow: <SamplePrevArrow />,
	responsive: [
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 3,
			},
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
			},
		},
	],
};

function HomeSlide() {
	return (
		<div className='row'>
			<div className='Slider_section'>
				<Slider {...settings}>

					<div className='item'>
						<SlideContent
						style={{ margin: "1rem" }}
						></SlideContent>
					</div>

					<div className='item'>
						<SlideContent
							style={{ margin: "1rem" }}
						></SlideContent>
					</div>

					<div className='item'>
						<SlideContent
							style={{ margin: "1rem" }}
						></SlideContent>
					</div>

					<div className='item'>
						<SlideContent
							style={{ margin: "1rem" }}
						></SlideContent>
					</div>

				</Slider>
			</div>
		</div>
	);
}

export default HomeSlide;
