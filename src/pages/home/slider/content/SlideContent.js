import React from 'react';

import "../content/SlideContent.css";

function SlideContent(props) {
  return (
    <div className='inner_content'>
      <h3>Title</h3>
      <ul className='custome-scroll'>
        <li><a href="javascript:void(0)">Lorem Ipsum is simply dummy text</a></li>
        <li><a href="javascript:void(0)">Lorem Ipsum is simply dummy text</a></li>
        <li><a href="javascript:void(0)">Lorem Ipsum is simply dummy text</a></li>
        <li><a href="javascript:void(0)">Lorem Ipsum is simply dummy text</a></li>
        <li><a href="javascript:void(0)">Lorem Ipsum is simply dummy text</a></li>
        <li><a href="javascript:void(0)">Lorem Ipsum is simply dummy text</a></li>
        <li><a href="javascript:void(0)">Lorem Ipsum is simply dummy text</a></li>

      </ul>

      <div className='center_side'>
        <button type='button' className='view_more'>view more</button>
      </div>
    </div>
  );
}

export default SlideContent;
