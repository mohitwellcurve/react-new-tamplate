
import React from 'react';

import "../home/Home.css";
import HomeSlide from './slider/HomeSlide';

function Home() {
  return (
    <div className='main'>
      <div className='container'>
        <HomeSlide ></HomeSlide>
      </div>
    </div>
  );
}

export default Home;
