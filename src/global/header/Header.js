
import React from 'react';

import "./Header.css";

function Header() {

  return (
    <div className='header'>
      <div className='container'>
        <div className='row'>

          <div className='col-md-2 left'>
            <a href="/"><img src={require('../../assets/images/global/header/logo.png')} /></a>
          </div>

          <div className='col-md-10 right'>
            <form action='#'>
              <fieldset>
                <select>
                  <option value={0}>All</option>
                </select>

                <div className='search-box'>
                  <input type="text" placeholder='Search here..' />
                  <button type="submit">Search</button>
                  <span className='close'>x</span>
                </div>

              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;
